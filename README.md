# Continuous Integration

Following [Gitlab Runner Instruction](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner).

1. Install and register gitlab runner for your project (use the token from Settings->Runners page of your project).
2. Choose Shell as the executor.
3. Create `gitlab-ci.yml` file and config your test steps.

*It will be good to use a separate EC2 instance for Gitlab Runner Only.*

# Continuous Deployment
**Disable the shared runner for your project firstly! Otherwise weird thing will happen (e.g. it will use docker image instead of shell executor even you config shell as the executor)**

*I am using [Capistrano](http://capistranorb.com/) for automatic deployment, other alternatives: [Deployer](https://deployer.org/) or AWS Code Deploy.*

**There are two keys we need to create**

1. The key for connecting gitlab runner with remote server (e.g. EC2 instance). Specifically, creating a ssh key pairs
in gitlab runner machine (should be /home/gitlab-runner/.ssh if gitlab-runner is the Linux user to run the build and test), add the public key to AWS instance (/home/deployer/.ssh/authorized_keys if deployer is the Linux user to run the deploy steps). In terms of the AWS instance, creating a separate Linux user (e.g. deployer).
Good references from [Digital Ocean](https://www.digitalocean.com/community/tutorials/how-to-use-capistrano-to-automate-deployments-getting-started) and
[AWS](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/managing-users.html)
2. The key for connecting the AWS instance with Gitlab project.
cd /home/deployer/.ssh, creating ssh key paris, then add the public key to Gitlab (Settings->deploy keys).

**Next**

**Don't need to install ruby, capistrano in your prod server, only install them in your gitlab runner server**

1. Install ruby, bundle, capistrano and use capistrano symfony recipe for deployment.
2. Run `bundle install`, `cap install`
3. Please check the project gravity-master as a reference.

# Details

After setup everything, we run `cap production deploy` as the user `deployer` of remote production server, if the `/var/www/html` folder
is belongs to `root` user and `www` group then we don't have write permission. Currently, I changed the group of this folder to `deployers` (the group which `deployer` is belongs to) and `chmod 775` for this folder.
So that we (as the Linux user `deployer`) has the right to read, write and execute for the `html` folder.

Alternatives, My team just use the same user:group to avoid any permission or owner change, in terms of the above case, use `deployer`:`deployers` for `/var/www/html` folder.

# Symfony3 Recipe
Here is a good article about some specific steps for [Symfony3 Capistrano](https://codestory.me/symfony3-deployment-capistrano-example/).